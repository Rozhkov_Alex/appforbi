﻿using System.IO;
using System.Text.RegularExpressions;
using testAppForBI.Models;

namespace testAppForBI.Helpers
{
    public static class ParseHelper
    {
        public static string GetJunOsCli(string path)
        {
            var resultString = "";

            using (var fs = new StreamReader(path))
            {
                while (true)
                {
                    string temp = fs.ReadLine();

                    if (temp == null) break;

                    resultString += temp + " ";
                }
            }

            return resultString;
        }    
    }
}
