﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Xml.Serialization;

namespace testAppForBI.Models
{
    [XmlRoot(ElementName = "application")]
    public class Application : BaseViewModel
    {
        [XmlElement(ElementName = "name")]
        [TableSettings(ColumnWidth = 30)]
        public string Name { get; set; }

        [XmlElement(ElementName = "protocol")]
        public string Protocol { get; set; }

        [XmlElement(ElementName = "destination-port")]
        [TableSettings(ColumnDisplayName = "Destination Port")]
        public string DestinationPort { get; set; }

        [XmlElement(ElementName = "source-port")]
        [TableSettings(ColumnDisplayName = "Source Port")]
        public string SourcePort { get; set; }

        [XmlElement(ElementName = "description")]
        public string Description { get; set; }

    }

    [XmlRoot(ElementName = "application-set")]
    public class ApplicationSet : BaseViewModel
    {
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "application")]
        public ObservableCollection<Application> Application { get; set; } = new ObservableCollection<Application>();

        [XmlElement(ElementName = "application-set")]
        public ApplicationSet Applicationset { get; set; }

    }

    [XmlRoot(ElementName = "applications")]
    public class Applications : BaseViewModel
    {
        [XmlElement(ElementName = "application")]
        public ObservableCollection<Application> Application { get; set; }

        [XmlElement(ElementName = "application-set")]
        public ObservableCollection<ApplicationSet> Applicationset { get; set; }

        public Applications()
        {
            Application = new ObservableCollection<Application>();
            Applicationset = new ObservableCollection<ApplicationSet>();
        }
    }

    
}
