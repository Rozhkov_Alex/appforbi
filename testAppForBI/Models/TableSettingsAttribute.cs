﻿using System;
using System.Collections.Generic;

namespace testAppForBI.Models
{
    public class TableSettingsAttribute : Attribute
    {
        public int ColumnWidth { get; set; }
        public string ColumnDisplayName { get; set; }
        public List<string> ColumnsDisplayName { get; set; }

        public TableSettingsAttribute()
        {
            ColumnWidth = 0;
            ColumnDisplayName = "";
        }

        public TableSettingsAttribute(Type type)
        {
            ColumnsDisplayName = new List<string>();

            var props = type.GetProperties();
            foreach (var prop in props)
            {
                var attribute =
                (TableSettingsAttribute)GetCustomAttribute(prop, typeof(TableSettingsAttribute));

                if (attribute != null)
                {
                    if (attribute.ColumnWidth != 0)
                    {
                        ColumnWidth = attribute.ColumnWidth;
                    }

                    if (attribute.ColumnDisplayName != "")
                    {
                        ColumnsDisplayName.Add(attribute.ColumnDisplayName);
                    }
                    else
                    {
                        ColumnsDisplayName.Add(prop.Name);
                    }
                }
                else
                {
                    ColumnsDisplayName.Add(prop.Name);
                }
            }

        }
    }
}
