﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Xml.Serialization;
using DevExpress.Mvvm;
using Microsoft.Win32;
using testAppForBI.Helpers;
using testAppForBI.Models;

namespace testAppForBI.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public Applications Applications { get; set; }

        public TableSettingsAttribute Settings { get; set; }


        public ICommand OpenDialogForImport
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    var ofd = new OpenFileDialog
                    {
                        Filter = null  //"JunOS application|*.cli" можно использовать чтобы не делать обработку
                    };

                    if (ofd.ShowDialog() == true)
                    {
                        if (Path.GetExtension(ofd.FileName) == ".cli")
                        {
                            var cli = ParseHelper.GetJunOsCli(ofd.FileName);
                            GetObjects(cli);

                        }
                        else
                        {
                            System.Windows.MessageBox.Show(@"Неверный формат файла", @"Ошибка");
                        }
                    }
                });
            }
            
        }

        public ICommand SaveDialogForExport
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    var sfd = new SaveFileDialog()
                    {
                        Filter = "JunOS XML|*.xml",
                        FileName = "untitle"
                    };

                    if (sfd.ShowDialog() == true)
                    {
                        // передаем в конструктор тип класса
                        var formatter = new XmlSerializer(typeof(Applications));

                        // получаем поток, куда будем записывать сериализованный объект
                        using (var fs = new FileStream(sfd.FileName, FileMode.OpenOrCreate))
                        {
                            formatter.Serialize(fs, Applications);
                        }
                    }
                    
                });
            }
        }

        public MainViewModel()
        {
            Applications = new Applications();

            Settings = new TableSettingsAttribute(typeof(Application));
        }


        private void GetObjects(string cli)
        {
            var app = new Application();
            var appSet = new ApplicationSet();

            var patternApp = @"ons application (?<name>\S*-\S*).(?<text>\S*.\S*)";
            var patternAppSet = @"ons application-set (?<name>\S*).(?<text>\S*.\S*)";

            var appRegex = new Regex(patternApp);
            var appSetRegex = new Regex(patternAppSet);


            for (var m = appRegex.Match(cli); m.Success; m = m.NextMatch())
            {
                app.Name = m.Groups["name"].Value;

                GetPropertyApplication(ref app, m.Groups["text"].Value);

                if (m.Groups["name"].Value != m.NextMatch().Groups["name"].Value)
                {
                    Applications.Application.Add(app);
                    app = new Application();
                }
            }

            for (var m = appSetRegex.Match(cli); m.Success; m = m.NextMatch())
            {
                appSet.Name = m.Groups["name"].Value;

                GetPropertyApplicationSet(ref appSet, m.Groups["text"].Value);

                if (m.Groups["name"].Value != m.NextMatch().Groups["name"].Value)
                {
                    Applications.Applicationset.Add(appSet);
                    appSet = new ApplicationSet();
                }
            }

        }
        
        private void GetPropertyApplication(ref Application obj, string str)
        {
            var protocolPattern = @"protocol \S*";
            var sourcePortPattern = @"source-port \S*";
            var destinationPortPattern = @"destination-port \S*";
            var descriptionPattern = @"description \S*";

            var protocol = Regex.Match(str, protocolPattern).Value.Replace("protocol ", "");
            var sourcePort = Regex.Match(str, sourcePortPattern).Value.Replace("source-port ", "");
            var destinationPort = Regex.Match(str, destinationPortPattern).Value.Replace("destination-port ", "");
            var description = Regex.Match(str, descriptionPattern).Value.Replace("description ", "");

            if (protocol != "") obj.Protocol = protocol;
            if (sourcePort != "") obj.SourcePort = sourcePort;
            if (destinationPort != "") obj.DestinationPort = destinationPort;
            if (description != "") obj.Description = description;

        }

        private void GetPropertyApplicationSet(ref ApplicationSet obj, string str)
        {
            var patternOne = @"application \S*";
            var patternTwo = @"application-set \S*";

            var name = Regex.Match(str, patternOne).Value.Replace("application ", "");

            if (name != "")
            {
                obj.Application.Add(new Application
                {
                    Name = name
                });
            }
            else
            {
                name = Regex.Match(str, patternTwo).Value.Replace("application-set ", "");
                obj.Applicationset = new ApplicationSet
                {
                    Name = name
                };
            }
        }

    }
}
